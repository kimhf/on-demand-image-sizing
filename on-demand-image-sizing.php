<?php
/*
Plugin Name:  On Demand Image Sizing
Plugin URI:
Description:  Experimental code for generating custom image sizes on demand. If you are using many custom image sizes (add_image_size(...)) to prevent the creation of all the sizes upon image upload. This will only generate a custom size the first time it is requested.
Version:      2.2.3
Author:
Author URI:
License:
*/

// https://gist.github.com/wkw/9c5f795c8187665b68cc

/* ======================================================================================
    //! -- On Demand Image Sizing --

    Experimental code for generating custom image sizes on demand.
      http://wordpress.stackexchange.com/a/124790

    From the author...
      "When an image is then requested in a particular size,
       which is not yet generated, it will be created only that once."
   ====================================================================================== */
add_filter('image_downsize', 'ml_media_downsize', 10, 3);
function ml_media_downsize($out, $id, $size) {
  $dims = null;
  $crop = false;
  $bw = false;

  // If it is a request for a full size image let WordPress handle it.
  if ('full' === $size) {
    return false;
  }

  if( is_array($size) ){
    // don't handle these for now. but if you want on-the-fly sizes, comment out
    // the return statement
    return false;

    $dims = $size;
    // create custom image size name consisting of widthXheight
    $size = sprintf("%dx%d", $size[0],$size[1]);
  }

  if (endsWith($size, '--bw')) {
    $bw = true;
    add_filter('wp_get_attachment_metadata', 'bw_filter_attachment_metadata', 10, 2);
  }

  $imagedata = wp_get_attachment_metadata($id);

  if (!$imagedata) {
    return false;
  }

  if ($bw) {
    remove_filter('wp_get_attachment_metadata', 'bw_filter_attachment_metadata', 10, 2);
  }

  if (isset($imagedata['sizes']) && is_array($imagedata['sizes']) && array_key_exists($size, $imagedata['sizes'])) {
    if ($bw) {
      // Find a fallback size if needed
      if (!is_array($imagedata['sizes'][$size])) {
        foreach ($imagedata['sizes'] as $key => $imagedataSize) {
          if (!is_array($imagedataSize)) {
            $size = $key;
            break;
          }
        }
      }

      if (is_array($imagedata['sizes'][$size])) {
        $att_url = wp_get_attachment_url($id);
        return array(dirname($att_url) . '/' . $imagedata['sizes'][$size]['file'], $imagedata['sizes'][$size]['width'], $imagedata['sizes'][$size]['height'], true);
      }

      return false;
    } else {
      // If image size exists and is not a bw image, let WP serve it like normally
      return false;
    }
  }

  // Check that the requested size exists, or abort
  if($dims === null) {
    global $_wp_additional_image_sizes;
    if (!isset($_wp_additional_image_sizes[$size])) {
        return false;
    }

    $dims = [];
    $dims[0] = $_wp_additional_image_sizes[$size]['width'];
    $dims[1] = $_wp_additional_image_sizes[$size]['height'];
  }

  $orig_w = $imagedata['width'];
  $orig_h = $imagedata['height'];
  $dest_w = $dims[0];
  $dest_h = $dims[1];
  $crop = $_wp_additional_image_sizes[$size]['crop'];

  $can_resize = image_resize_dimensions($orig_w, $orig_h, $dest_w, $dest_h, $crop);

  if (!$can_resize) {
    return;
  }

  // Make the new thumb
  if($dims){
    if ($bw) {
      if (!$resized = image_make_intermediate_size_bw(
        get_attached_file($id),
        $dims[0],
        $dims[1],
        $crop,
        $bw
      )){
        return;
      }
    } else {
      if (!$resized = image_make_intermediate_size(
          get_attached_file($id),
          $dims[0],
          $dims[1],
          $crop
      )){
        return;
      }
    }
  }

  // Save image meta, or WP can't see that the thumb exists now
  if ($resized) {
    if ($bw) {
      $imagedata['sizes'][$size] = $resized;
      wp_update_attachment_metadata_bw_sizes($id, $imagedata['sizes']);
    } else {
      $imagedata['sizes'][$size] = $resized;
      wp_update_attachment_metadata($id, $imagedata);
    }

    // Return the array for displaying the resized image
    $att_url = wp_get_attachment_url($id);
    return array(dirname($att_url) . '/' . $resized['file'], $resized['width'], $resized['height'], true);
  }
  return;
}

add_filter('intermediate_image_sizes_advanced', 'ml_media_prevent_resize_on_upload');
function ml_media_prevent_resize_on_upload($sizes) {
    // Removing these defaults might cause problems, so we don't, though you could
    // probably set them to all be the same dimensions and rely on custom image sizes
    // for everything else
    return array(
        'thumbnail' => $sizes['thumbnail'],
        'medium'    => $sizes['medium'],
        'large'     => $sizes['large']
    );
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

/**
 * Resizes an image to make a thumbnail or intermediate size, in color or black/white.
 * Based on wp cores image_make_intermediate_size()
 *
 * The returned array has the file size, the image width, and image height. The
 * filter 'image_make_intermediate_size' can be used to hook in and change the
 * values of the returned array. The only parameter is the resized file path.
 *
 * @since 1.1.0
 *
 * @param string $file   File path.
 * @param int    $width  Image width.
 * @param int    $height Image height.
 * @param bool   $crop   Optional. Whether to crop image to specified width and height or resize.
 *                       Default false.
 * @param bool   $bw     Optional. Whether to use grayscale filter on the generated image.
 * @return false|array False, if no image was created. Metadata array on success.
 */
function image_make_intermediate_size_bw( $file, $width, $height, $crop = false, $bw = false ) {
	if ( $width || $height ) {
		$editor = wp_get_image_editor( $file );

    $filename = null;

		if ( is_wp_error( $editor ) || is_wp_error( $editor->resize( $width, $height, $crop ) ) )
			return false;

    if ( $bw ) {
      $filename = $editor->generate_filename( '-bw-'.$editor->get_suffix(), null, null );
    }

		$resized_file = $editor->save($filename);

		if ( ! is_wp_error( $resized_file ) && $resized_file ) {

      if ( $bw && $resized_file['mime-type'] == 'image/jpeg' ) {
        $im = imagecreatefromjpeg($resized_file['path']);

        if ( $im && imagefilter($im, IMG_FILTER_GRAYSCALE) ) {
            //Image converted to grayscale.

            imagejpeg($im, $resized_file['path']);
        } else {
            //Conversion to grayscale failed.
        }
      } else if ( $bw && $resized_file['mime-type'] == 'image/png' ) {
        $im = imagecreatefrompng($resized_file['path']);

        if ( $im && imagefilter($im, IMG_FILTER_GRAYSCALE) ) {
            //Image converted to grayscale.

            imagepng($im, $resized_file['path']);
        } else {
            //Conversion to grayscale failed.
        }
      }

			unset( $resized_file['path'] );
			return $resized_file;
		}
	}
	return false;
}

/**
 * Update bw image metadata for an attachment. Based on wp core wp_update_attachment_metadata()
 *
 * @since 1.2.0
 *
 * @param int   $post_id Attachment ID.
 * @param array $data    Attachment data.
 * @return int|bool False if $post is invalid.
 */
function wp_update_attachment_metadata_bw_sizes($post_id, $sizes)
{
	$post_id = (int) $post_id;
	if (!$post = get_post($post_id))
		return false;

	return update_post_meta($post->ID, '_wp_attachment_metadata_bw_sizes', $sizes);
}

/**
 * Retrieve bw attachment meta info for attachment ID.
 *
 * @since 1.2.0
 *
 * @param int  $post_id    Attachment ID. Default 0.
 * @param bool $unfiltered Optional. If true, filters are not run. Default false.
 * @return mixed Attachment meta field. False on failure.
 */
function bw_filter_attachment_metadata($data, $post_id = 0)
{
	$post_id = (int) $post_id;
	if (!$post = get_post($post_id))
		return $data;

	$bw_sizes = get_post_meta($post->ID, '_wp_attachment_metadata_bw_sizes', true);

  if (!$bw_sizes || !is_array($bw_sizes)) {
    $bw_sizes = [];
  }

  if (is_array($data)) {
    $data['sizes'] = $bw_sizes;
  }

  return $data;
}
